<?php

namespace Drupal\flag_route\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * List builder for Flag Route entries.
 *
 * @package Drupal\flag_route\Controller
 */
class FlagRouteListBuilder extends EntityListBuilder {

  /**
   * {@inheritDoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['link'] = $this->t('Path');
    $header['language'] = $this->t('Language');
    $header['uid'] = $this->t('Author');
    $header['status'] = $this->t('Published');

    $header += parent::buildHeader();

    return $header;
  }

  /**
   * {@inheritDoc}
   *
   * @param \Drupal\flag_route\FlagRouteInterface $entity
   *   Flag Route entity.
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['link'] = $entity->link();
    $row['language'] = $entity->getLanguage()->getName();
    $row['uid'] = $entity->getOwner()->get('name')->getString();
    $row['status'] = $entity->isPublished() ? $this->t('Yes') : $this->t('No');

    $row += parent::buildRow($entity);

    return $row;
  }

}
