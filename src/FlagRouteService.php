<?php

namespace Drupal\flag_route;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Flag Route Service.
 */
class FlagRouteService implements FlagRouteServiceInterface {

  /**
   * The current user instance.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The Language Manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The Constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The Language Manager.
   */
  public function __construct(AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack,
    LanguageManagerInterface $language_manager
  ) {
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function matchRoute(): array {
    $current_request = $this->requestStack->getCurrentRequest();

    // @todo Investigate option to attach query string.
    $path = $current_request->getPathInfo();
    $path = 'internal:/' . ltrim(Xss::filter($path), '/');

    $default_language = $this->languageManager->getDefaultLanguage()->getId();
    $current_language = $this->languageManager->getCurrentLanguage()->getId();

    $storage = $this->entityTypeManager->getStorage('flag_route');
    $query = $storage
      ->getQuery()
      ->accessCheck();

    $acceptable_languages = [
      $current_language,
      $default_language,
      Language::LANGCODE_NOT_SPECIFIED,
      Language::LANGCODE_NOT_APPLICABLE,
    ];

    $query->condition('link', $path);
    $query->condition('language', $acceptable_languages, 'IN');
    $query->condition('status', TRUE);

    $flag_routes = $query->execute();
    $flag_routes = $storage->loadMultiple($flag_routes);

    if (!empty($flag_routes)) {
      foreach ($acceptable_languages as $acceptable_language) {
        $flag_route_filtered = array_filter($flag_routes, function (FlagRouteInterface $flag_route) use ($acceptable_language) {
          return $flag_route->language()->getId() === $acceptable_language;
        });

        if (!empty($flag_route_filtered)) {
          /** @var \Drupal\flag_route\FlagRouteInterface $flag_route */
          $flag_route = array_shift($flag_route_filtered);
          // Return matching.
          return [
            'entity_type_id' => $flag_route->getEntityTypeId(),
            'id' => $flag_route->id(),
          ];
        }
      }
    }

    // Return default if not found matching.
    return [
      'entity_type_id' => NULL,
      'id' => NULL,
    ];
  }

}
