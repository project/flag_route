<?php

namespace Drupal\flag_route\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Create form to provide flag_route.
 *
 * @package Drupal\flag_route\Form
 */
class FlagRouteForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.flag_route.canonical');

    parent::submitForm($form, $form_state);
  }

}
