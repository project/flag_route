<?php

namespace Drupal\flag_route;

/**
 * Flag Route Service Interface.
 */
interface FlagRouteServiceInterface {

  /**
   * Get matched flag route entity and ID.
   *
   * @return array
   *   Return matched flag route entity and ID.
   */
  public function matchRoute(): array;

}
