<?php

namespace Drupal\flag_route;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Flag Route Entity Interface.
 */
interface FlagRouteInterface extends ContentEntityInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Get Link Uri.
   *
   * @return string
   *   Link Uri.
   */
  public function link(): string;

  /**
   * Get Language.
   *
   * @return \Drupal\Core\Language\LanguageInterface
   *   Attached Language.
   */
  public function getLanguage(): LanguageInterface;

}
