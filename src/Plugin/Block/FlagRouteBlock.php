<?php

namespace Drupal\flag_route\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\flag\FlagLinkBuilderInterface;
use Drupal\flag\FlagServiceInterface;
use Drupal\flag_route\FlagRouteServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Flag Route Block.
 *
 * @Block(
 *   id = "flag_route",
 *   admin_label = @Translation("Flag Route Block"),
 *   category = @Translation("Flag")
 * )
 */
class FlagRouteBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Flag service.
   *
   * @var \Drupal\flag\FlagServiceInterface
   */
  protected FlagServiceInterface $flag;

  /**
   * Flag link builder service.
   *
   * @var \Drupal\flag\FlagLinkBuilderInterface
   */
  protected FlagLinkBuilderInterface $flagLinkBuilder;

  /**
   * Flag route service.
   *
   * @var \Drupal\flag_route\FlagRouteServiceInterface
   */
  protected FlagRouteServiceInterface $flagRouteService;

  /**
   * The Constructor.
   *
   * @param array $configuration
   *   The configuration.
   * @param mixed $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\flag\FlagServiceInterface $flag
   *   The flag service.
   * @param \Drupal\flag\FlagLinkBuilderInterface $flag_link_builder
   *   The flag link builder service.
   * @param \Drupal\flag_route\FlagRouteServiceInterface $flag_route_service
   *   The flag route service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    FlagServiceInterface $flag,
    FlagLinkBuilderInterface $flag_link_builder,
    FlagRouteServiceInterface $flag_route_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->flag = $flag;
    $this->flagLinkBuilder = $flag_link_builder;
    $this->flagRouteService = $flag_route_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('flag'),
      $container->get('flag.link_builder'),
      $container->get('flag_route.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();

    $configuration['flag'] = NULL;

    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['flag'] = [
      '#type' => 'select',
      '#title' => $this->t('Flag'),
      '#options' => $this->getFlagTypes(),
      '#required' => TRUE,
      '#default_value' => $config['flag'],
      '#description' => $this->t('Select a type of <a href=":flag" target="_blank">Flag</a>', [
        ':flag' => Url::fromRoute('entity.flag.collection')->toString(),
      ]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['flag'] = $form_state->getValue('flag');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): ?array {
    $config = $this->getConfiguration();

    /** @var \Drupal\flag\Entity\Flag $flag */
    $flag = $this->flag->getFlagById($config['flag']);

    if (empty($flag)) {
      return NULL;
    }

    [
      'entity_type_id' => $flag_route_type_id,
      'id' => $flag_route_id,
    ] = $this->flagRouteService->matchRoute();

    if (empty($flag_route_id)) {
      return NULL;
    }

    return $this->flagLinkBuilder->build(
      $flag_route_type_id,
      $flag_route_id,
      $config['flag']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * Get all flag types.
   *
   * @return array
   *   Flag types array.
   */
  protected function getFlagTypes(): array {
    $flags = $this->flag->getAllFlags();
    $flag_list = [];

    foreach ($flags as $flag) {
      $flag_list[$flag->id()] = $flag->label();
    }

    return $flag_list;
  }

}
