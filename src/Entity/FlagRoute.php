<?php

namespace Drupal\flag_route\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\flag_route\FlagRouteInterface;
use Drupal\link\LinkItemInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the Flag Route entity.
 *
 * @ContentEntityType(
 *   id = "flag_route",
 *   label = @Translation("Flag Route"),
 *   base_table = "flag_route",
 *   data_table = "flag_route_field_data",
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\flag_route\Form\FlagRouteForm",
 *       "add" = "Drupal\flag_route\Form\FlagRouteForm",
 *       "edit" = "Drupal\flag_route\Form\FlagRouteForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\flag_route\Controller\FlagRouteListBuilder",
 *     "views_data" = "Drupal\flag_route\FlagRouteViewsData",
 *   },
 *   admin_permission = "administer flag_route configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "label" = "label",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/flags/flag-route",
 *     "edit-form" = "/admin/structure/flags/flag-route/{flag_route}/edit",
 *     "delete-form" = "/admin/structure/flags/flag-route/{flag_route}/delete",
 *     "items-list" = "/admin/structure/flags/flag-route/{flag_route}/list",
 *   }
 * )
 */
class FlagRoute extends ContentEntityBase implements FlagRouteInterface {

  use EntityOwnerTrait;

  use EntityPublishedTrait;

  /**
   * Use the Flag Route globally.
   */
  const SCOPE_GLOBAL = 'global';

  /**
   * Use the Flag Route personally.
   */
  const SCOPE_PERSONAL = 'personal';

  /**
   * {@inheritDoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('ID'))
      ->setDescription(new TranslatableMarkup('The ID of the flag route entity.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(new TranslatableMarkup('UUID'))
      ->setDescription(new TranslatableMarkup('The UUID of the flag route entity.'))
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Authored on'))
      ->setDescription(new TranslatableMarkup('The time that the flag route was created.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
      ]);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Authored by'))
      ->setDescription(new TranslatableMarkup('The username of the flag route author.'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setLabel(new TranslatableMarkup('Authored by'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 1,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Label'))
      ->setDescription(new TranslatableMarkup('The label of flag route entity.'))
      ->setDefaultValue('')
      ->setSettings([
        'max_length' => 255,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string',
        'weight' => 0,
      ]);

    $fields['link'] = BaseFieldDefinition::create('link')
      ->setLabel(new TranslatableMarkup('Path'))
      ->setDescription(new TranslatableMarkup('The path.'))
      ->setRequired(TRUE)
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_GENERIC,
        'title' => DRUPAL_DISABLED,
      ])
      ->setDisplayOptions('form', [
        'type' => 'link',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['language'] = BaseFieldDefinition::create('language')
      ->setLabel(new TranslatableMarkup('Language'))
      ->setDescription(new TranslatableMarkup('The route language.'))
      ->setDisplayOptions('form', [
        'type' => 'language_select',
        'weight' => 0,
      ]);

    $fields['scope'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Scope'))
      ->setRequired(TRUE)
      ->setDefaultValue(self::SCOPE_GLOBAL)
      ->setSetting('disabled', TRUE)
      ->setSetting('allowed_values', [
        self::SCOPE_GLOBAL => new TranslatableMarkup('Use Globally'),
        self::SCOPE_PERSONAL => new TranslatableMarkup('Use Privately'),
      ]);
    // ->setDisplayOptions('form', [
    //   'type' => 'options_select',
    //   'weight' => 0,
    // ])
    // ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Published'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 2,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  public function link(): string {
    return $this->get('link')->uri;
  }

  /**
   * {@inheritDoc}
   */
  public function getLanguage(): LanguageInterface {
    $language_manager = \Drupal::languageManager();
    $language_id = $this->get('language')->first()->getString();

    return $language_manager->getLanguage($language_id);
  }

  /**
   * {@inheritDoc}
   */
  public function toUrl($rel = 'canonical', array $options = []) {
    if ($rel === 'canonical' && !$this->get('link')->isEmpty()) {
      $url = Url::fromUri($this->link(), $options);
    }
    else {
      $url = parent::toUrl($rel, $options);
    }

    return $url;
  }

}
