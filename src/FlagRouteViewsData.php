<?php

namespace Drupal\flag_route;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\EntityViewsData;

/**
 * Flag Route Views Data.
 */
class FlagRouteViewsData extends EntityViewsData {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['flag_route']['table']['group'] = $this->t('Flag Route');

    $data['flag_route']['label']['field']['default_formatter_settings'] = ['link_to_entity' => TRUE];

    $data['flag_route']['flag_relationship'] = [
      'title' => $this->t('Flag'),
      'help' => $this->t('The relation to Flag'),
      'relationship' => [
        'group' => $this->t('Flag'),
        'label' => $this->t('Flags'),
        'base' => 'flagging',
        'base field' => 'entity_id',
        'relationship field' => 'id',
        'id' => 'flag_relationship',
        'flaggable' => 'flag_route',
      ],
    ];

    $data['flagging']['flag_route'] = [
      'title' => $this->t('Flag Route'),
      'help' => $this->t('The relation to Flag Route'),
      'relationship' => [
        'group' => $this->t('Flag Route'),
        'label' => $this->t('Flag Route'),
        'base' => 'flag_route',
        'base field' => 'id',
        'relationship field' => 'entity_id',
        'extra' => [
          [
            'field' => 'entity_type',
            'value' => 'flag_route',
            'table' => 'flagging',
          ],
        ],
        'id' => 'standard',
      ],
    ];

    $data['flagging']['node_field_data'] = [
      'title' => $this->t('Content'),
      'help' => $this->t('The relation to Content Type'),
      'relationship' => [
        'group' => $this->t('Content'),
        'label' => $this->t('Content'),
        'base' => 'node_field_data',
        'base field' => 'nid',
        'relationship field' => 'entity_id',
        'extra' => [
          [
            'field' => 'entity_type',
            'value' => 'node',
            'table' => 'flagging',
          ],
        ],
        'id' => 'standard',
      ],
    ];

    return $data;
  }

}
